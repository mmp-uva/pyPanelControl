"""Controllers managed the interaction between model and view"""

from julesTk import controller
from view import MainView
from view.list import DeviceListView
from view.panel import PanelView
from connection import *

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class MainController(controller.ViewController):

    VIEW_CLASS = MainView

    def _start(self):
        super(MainController, self)._start()
        self.show_list()

    def load_list(self):
        if not self.application.has_controller("device_list"):
            c = DeviceListController(self).prepare()
            self.application.add_controller("device_list", c)
        return self.application.get_controller("device_list")

    def show_list(self):
        self.load_list().start()

    def close_list(self):
        if self.application.has_controller("device_list"):
            self.load_list().stop()
            self.application.remove_controller("device_list")

    def load_device(self, model=None):
        if not self.application.has_controller("device"):
            c = PanelController(self, model=model).prepare()
            self.application.add_controller("device", c)
        return self.application.get_controller("device")

    def show_device(self, model=None):
        self.load_device(model).start()

    def close_device(self):
        if self.application.has_controller("device"):
            self.load_device().stop()
            self.application.remove_controller("device")


class DeviceListController(controller.ViewController):

    VIEW_CLASS = DeviceListView

    def __init__(self, parent, view=None):
        super(DeviceListController, self).__init__(parent=parent, view=view)
        self._devices = []

    @property
    def view(self):
        """The view of this controller

        :return:
        :rtype: pyPanelControl.view.DeviceListView
        """
        return super(DeviceListController, self).view

    @property
    def devices(self):
        """ A list of all connected OpalKelly devices

        :return:
        :rtype: list[dict[str, str]]
        """
        return self._devices

    def _prepare(self):
        super(DeviceListController, self)._prepare()
        return self

    def _start(self):
        super(DeviceListController, self)._start()
        self.refresh_devices()

    def refresh_devices(self):
        devices = [{"serial": "1234", "model": "Fake"}]
        devices.extend(list_device_info())
        self._devices = devices
        self.refresh_view()

    def refresh_view(self):
        self.view.clear_devices()
        for d in self._devices:
            self.view.add_device(d)

    def select_device(self, index):
        if 0 > index or index > len(self._devices):
            raise IndexError("Invalid index")
        # create model and start Controller
        device = self.devices[index]
        if str(device["model"]).lower() != "Fake".lower():
            connection = Connection(PANEL_BIT_FILE, device["serial"])
        else:
            connection = FakeConnection(PANEL_BIT_FILE, device["serial"])
        # start controller
        self.parent.close_list()
        self.parent.show_device(connection)


class PanelController(
    controller.ViewController, controller.ModelController
):

    VIEW_CLASS = PanelView

    def __init__(self, parent, view=None, model=None):
        controller.ViewController.__init__(self, parent=parent, view=view)
        controller.ModelController.__init__(self, parent=parent, model=model)

    @property
    def model(self):
        """ Model of this controller

        :return:
        :rtype: pyPanelControl.connection.Connection
        """
        return super(PanelController, self).model

    @model.setter
    def model(self, m):
        self._set_model(m)

    def _start(self, model=None):
        if isinstance(model, Connection):
            self.model = model
        if not isinstance(self.model, Connection):
            raise ValueError("Invalid connection attached")
        self.connect()
        return super(PanelController, self)._start()

    def _stop(self):
        self.disconnect()

    def update_panel(self, panel):
        pass

    def update_led(self, led):
        pass

    def connect(self):
        self.model.connect()

    def disconnect(self):
        if self.has_model():
            self.model.disconnect()

    def set_intensity(self, lamp, led, intensity):
        return self.model.set_intensity(lamp, led, intensity)

    def set_pulse_width(self, lamp, led, intensity):
        return self.model.set_pulse_width(lamp, led, intensity)
