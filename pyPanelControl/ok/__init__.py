#-------------------------------------------------------------------------
# __init__.py
#
# The presence of this file turns this directory into a Python package.
#-------------------------------------------------------------------------

# from . import __version__
# __version__ = __version__.VERSION_STRING
__version__ = "1.0.0.0"

# Load the package namespace with the core classes and such
from .ok import *

