
from julesTk.view import viewset

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class MainView(viewset.ViewSet):

    def _prepare(self):
        # resize parent with window
        self.configure_row(self.parent, 0)
        self.configure_column(self.parent, 0)
        # resize frame with window
        self.configure_grid(self)
        self.configure_column(self, 0)
        self.configure_row(self, 0)

    def _show(self):
        super(MainView, self)._show()
