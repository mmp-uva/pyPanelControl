"""Views"""
from julesTk import view
from julesTk.utils.modals import QuestionBox

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class PanelView(view.View):

    LED_COUNT = 8

    @property
    def controller(self):
        """Return the controller managing this view

        :return:
        :rtype: pyPanelControl.controller.PanelController
        """
        return super(PanelView, self).controller

    @property
    def lamp(self):
        result = 0
        try:
            result = int(self.get_variable("lamp").get())
        except ValueError:
            pass
        return result

    @property
    def status(self):
        return self.get_variable("status").get()

    @status.setter
    def status(self, msg):
        self.get_variable("status").set(msg)

    def _prepare(self):
        self.configure_grid(self)
        self.configure_column(self, 0)
        self.configure_row(self, [0, 1, 2])
        fmh = view.ttk.Frame(self)
        # view.ttk.Style().configure("Header.TFrame", background="green")
        # fmh.configure(style="Header.TFrame", relief=view.tk.SUNKEN)
        self.configure_grid(fmh, row=0)
        self._prepare_header(fmh)
        fmb = view.ttk.Frame(self)
        # view.ttk.Style().configure("Body.TFrame", background="yellow")
        # fmb.configure(style="Body.TFrame", relief=view.tk.SUNKEN)
        self.configure_grid(fmb, row=1)
        self._prepare_body(fmb)
        fmf = view.ttk.Frame(self)
        # view.ttk.Style().configure("Footer.TFrame", background="red")
        # fmf.configure(style="Footer.TFrame", relief=view.tk.SUNKEN)
        self.configure_grid(fmf, row=2)
        self._prepare_footer(fmf)
        self.status = ""

    def _prepare_header(self, parent):
        self.configure_column(parent, 0)
        # add title
        var = self.add_variable("title", view.tk.StringVar())
        lbt = view.ttk.Label(parent, textvariable=var)
        self.add_widget("title", lbt)
        self.configure_grid(lbt, row=0, column=0, columnspan=3, padx=5, pady=10)
        # add lamp label
        frm = view.ttk.Frame(parent)
        self.configure_grid(frm, row=1, padx=10)
        lbl = view.ttk.Label(frm, text="Lamp")
        self.add_widget("lbl_lamp", lbl)
        self.configure_grid(lbl, row=0, column=0)
        # add lamp select box
        lamps = ["Lamp {}".format(i) for i in range(8)]
        var = self.add_variable("lamp", view.tk.StringVar())
        oml = view.ttk.OptionMenu(
            frm, var, lamps[0], *lamps,
            command=lambda x: self.process_lamp()
        )
        self.add_widget("lamp", oml)
        self.configure_grid(oml, row=0, column=1)

    def _prepare_body(self, parent):
        self.configure_column(parent, range(4), uniform="leds_col")
        self.configure_row(parent, range(2), uniform="leds_row")
        for idx in range(self.LED_COUNT):
            frm = view.ttk.Frame(parent)
            self.configure_column(frm, [1, 2], uniform="led_%s" % idx)
            self.configure_row(frm, [0, 1, 2, 3])
            self.add_widget("frm_led_%s" % idx, frm)
            self._prepare_led(frm, idx)
            self.configure_grid(frm, row=idx // 4, column=idx % 4, pady=5, padx=5)

    def _prepare_footer(self, parent):
        var = self.add_variable("status", view.tk.StringVar())
        lbs = view.ttk.Label(
            parent, textvariable=var,
            relief=view.tk.SUNKEN
        )
        self.add_widget("status", lbs)
        lbs.pack(side=view.tk.BOTTOM, fill=view.tk.X)

    def _prepare_led(self, parent, index):
        """Prepare the widgets for one LED"""
        if parent is None:
            parent = self
        # add label with led index and label
        lbi = view.ttk.Label(
            parent, text="LED", anchor=view.tk.CENTER
        )
        self.add_widget("index_%s" % index, lbi)
        self.configure_grid(
            lbi, row=0, column=0, padx=5
        )
        var = self.add_variable(
            "label_%s" % index, view.tk.StringVar(self, index)
        )
        lbl_l = view.ttk.Label(
            parent, textvariable=var, relief=view.tk.SUNKEN,
            width=7
        )
        lbl_l.bind(
            "<Double-Button-1>", lambda x, y=index: self.change_label(y)
        )
        self.configure_grid(
            lbl_l, row=0, column=1, columnspan=2, padx=5
        )
        bte = view.ttk.Button(
            parent, text="Edit",
            command=lambda x=index: self.change_label(x)
        )
        self.configure_grid(
            bte, row=0, column=3, padx=5
        )
        # add vertical slider for pulse width (left)
        pwm = self.add_variable(
            "pwm_%s" % index, view.tk.IntVar()
        )
        lbpwm = view.ttk.Label(parent, text="Pulse Width")
        self.configure_grid(lbpwm, row=2, column=0, columnspan=2, padx=5)
        slpwm = view.tk.Scale(
            parent, variable=pwm, from_=1000, to=0, resolution=1,
            command=lambda x, y=index: self.process_pwm(y),
            orient=view.tk.VERTICAL
        )
        self.add_widget("pwm_%s" % index, slpwm)
        self.configure_grid(slpwm, row=3, column=0, columnspan=2, padx=5)
        # add vertical slider for intensity (right)
        intensity = self.add_variable(
            "intensity_%s" % index, view.tk.IntVar()
        )
        lbint = view.ttk.Label(parent, text="Intensity")
        self.configure_grid(lbint, row=2, column=2, columnspan=2, padx=5)
        slint = view.tk.Scale(
            parent, variable=intensity, from_=1000, to=0, resolution=1,
            command=lambda x, y=index: self.process_intensity(y),
            orient=view.tk.VERTICAL
        )
        self.add_widget("intensity_%s" % index, slint)
        self.configure_grid(slint, row=3, column=2, columnspan=2, padx=5)

    def get_pwm(self, led_idx):
        return self.get_variable("pwm_%s" % led_idx).get()

    def set_pwm(self, led_idx, value):
        self.get_variable("pwm_%s" % led_idx).set(
            value
        )

    def process_pwm(self, led_idx):
        label = self.get_label(led_idx)
        if label in (None, ""):
            label = led_idx
        pwm = self.get_pwm(led_idx)
        # apply intensity
        self.controller.set_pulse_width(self.lamp, led_idx, pwm)
        self.status = "Set pulse width of LED {} to {}".format(
            label, pwm
        )

    def get_intensity(self, led_idx):
        return self.get_variable("intensity_%s" % led_idx).get()

    def set_intensity(self, led_idx, value):
        self.get_variable("intensity_%s" % led_idx).set(
            value
        )

    def process_intensity(self, led_idx):
        label = self.get_label(led_idx)
        if label in (None, ""):
            label = led_idx
        intensity = self.get_intensity(led_idx)
        # apply intensity
        self.controller.set_intensity(self.lamp, led_idx, intensity)
        self.status = "Set intensity of LED {} to {}".format(
            label, intensity
        )

    def get_label(self, led_idx):
        return self.get_variable("label_%s" % led_idx).get()

    def set_label(self, led_idx, name):
        return self.get_variable("label_%s" % led_idx).set(
            name
        )

    def change_label(self, led_idx):
        old = self.get_label(led_idx)
        if old in (None, ""):
            old = led_idx
        new = QuestionBox.ask(self.controller, "New label", default=old)
        self.set_label(led_idx, new)
        self.status = "Change label of LED {} to {}".format(
            old, new
        )

    def reset(self):
        for led in range(self.LED_COUNT):
            self.set_intensity(led, 0)
            self.set_pwm(led, 0)

    def process_lamp(self):
        # reset view
        self.reset()
        self.status = "Change lamp index to: {}".format(self.lamp)
