
from julesTk import view

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class DeviceListView(view.View):

    @property
    def controller(self):
        """Return the controller managing this view

        :return:
        :rtype: pyPanelControl.controller.DeviceListController
        """
        return super(DeviceListView, self).controller

    @property
    def title(self):
        return self.get_variable("title").get()

    @title.setter
    def title(self, msg):
        self.get_variable("title").set(msg)

    @property
    def status(self):
        return self.get_variable("status").get()

    @status.setter
    def status(self, msg):
        self.get_variable("status").set(msg)

    def _prepare(self):
        self.configure_column(self, [0, 1, 2], uniform="foo")
        self.configure_row(self, [0, 1, 2])
        # add title
        var = self.add_variable("title", view.tk.StringVar())
        lbt = view.ttk.Label(self, textvariable=var)
        self.add_widget("title", lbt)
        self.configure_grid(lbt, row=0, column=0, columnspan=3, pady=10)
        # add list box
        lb = view.tk.Listbox(self, selectmode=view.tk.SINGLE)
        lb.bind("<Double-Button-1>", lambda x: self.process_ok())
        self.add_widget("devices", lb)
        self.configure_grid(lb, row=1, column=0, columnspan=3)
        # add close button
        btc = view.ttk.Button(self, text="Close", command=self.process_close)
        self.add_widget("close", btc)
        btc.grid(row=2, column=0)
        # add refresh button
        btr = view.ttk.Button(self, text="Refresh", command=self.refresh)
        self.add_widget("refresh", btr)
        btr.grid(row=2, column=1)
        # add ok button
        bto = view.ttk.Button(self, text="Select", command=self.process_ok)
        self.add_widget("ok", bto)
        bto.grid(row=2, column=2)
        # add statusbar
        var = self.add_variable("status", view.tk.StringVar())
        lbs = view.ttk.Label(self, textvariable=var, relief=view.tk.SUNKEN)
        self.add_widget("status", lbs)
        self.configure_grid(lbs, row=3, column=0, columnspan=3)
        self.title = "Select a Panel"
        self.status = ""

    def _show(self):
        super(DeviceListView, self)._show()
        self.configure_grid(self)
        self.refresh()

    def process_close(self):
        self.controller.application.stop()

    def refresh(self):
        self.controller.refresh_devices()

    def clear_devices(self):
        self.get_widget("devices").delete(0, view.tk.END)

    def add_device(self, d):
        if not isinstance(d, dict):
            raise TypeError("Invalid device type, expected dictionary not: {}".format(type(d)))
        msg = "{serial} (Model: {model})".format(**d)
        self.get_widget("devices").insert(view.tk.END, msg)

    def process_ok(self):
        selection = self.get_widget("devices").curselection()
        if len(selection) > 0:
            self.controller.select_device(selection[0])
        else:
            self.status = "Please select a device"
