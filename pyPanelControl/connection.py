"""Driver for the Panel"""

from julesTk import ThreadSafeObject
from ok import ok
import os
import pkg_resources

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"

PANEL_BIT_FILE = pkg_resources.resource_filename(__name__, "ok/ow_pwm.bit")


def count_devices(device=None):
    """Counts the number of connected OpalKelly Devices

    :param device: pyPanelControl.ok.ok.okCFrontPanel or None
    :rtype: int
    """
    if device is None:
        device = ok.okCFrontPanel()
    return device.GetDeviceCount()


def list_device_serial(device=None):
    """List the serial numbers of all connected OpalKelly devices

    :param device: pyPanelControl.ok.ok.okCFrontPanel or None
    :rtype: list[str]
    """
    if device is None:
        device = ok.okCFrontPanel()
    results = []
    for i in range(count_devices(device)):
        results.append(device.GetDeviceListSerial(i))
    return results


def list_device_model(device=None):
    """List the model numbers of all connected OpalKelly devices

    :param device: pyPanelControl.ok.ok.okCFrontPanel or None
    :rtype: list[str]
    """
    if device is None:
        device = ok.okCFrontPanel()
    results = []
    for i in range(count_devices(device)):
        results.append(device.GetDeviceListModel(i))
    return results


def list_device_info(device=None):
    """List the model and serial numbers of all connected OpalKelly devices

    :param device: pyPanelControl.ok.ok.okCFrontPanel or None
    :rtype: list[dict[str, str]]
    """
    if device is None:
        device = ok.okCFrontPanel()
    results = []
    for i in range(count_devices(device)):
        info = {
            "serial": device.GetDeviceListSerial(i),
            "model": device.GetDeviceListModel(i)
        }
        results.append(info)
    return results


class Connection(ThreadSafeObject):

    def __init__(self, bit_file, serial_nr=""):
        super(Connection, self).__init__()
        self._device = ok.okCFrontPanel()
        self._bit_file = bit_file
        self._serial_nr = serial_nr

    @property
    def device(self):
        with self.lock:
            result = self._device
        return result

    @ThreadSafeObject.thread_safe
    def is_connected(self):
        return self.device.IsFrontPanelEnabled()

    @property
    def bit_file(self):
        with self.lock:
            result = self._bit_file
        return result

    @bit_file.setter
    def bit_file(self, bit_file):
        if os.path.exists(bit_file):
            with self.lock:
                self._bit_file = bit_file

    @property
    def serial_nr(self):
        with self.lock:
            result = self._serial_nr
        return result

    @serial_nr.setter
    def serial_nr(self, serial):
        with self.lock:
            self._serial_nr = serial

    @ThreadSafeObject.thread_safe
    def connect(self, bit_file=None, serial_nr=None):
        result = False
        if bit_file is None:
            bit_file = self.bit_file
        if serial_nr is None:
            serial_nr = self.serial_nr
        if serial_nr is None:
            serial_nr = ""
        if self.device.OpenBySerial(serial_nr) >= 0:
            self.serial_nr = serial_nr
            try:
                result = self.configure(bit_file)
            except ConnectionException:
                result = False
            finally:
                if result is False:
                    self.disconnect()
        return result

    @ThreadSafeObject.thread_safe
    def configure(self, bit_file):
        if bit_file is None:
            raise ConnectionException("No bit file set")
        if not os.path.exists(bit_file):
            raise ConnectionException("Bit file location does not exist")
        self.device.ConfigureFPGA(bit_file)
        self.bit_file = bit_file

    @ThreadSafeObject.thread_safe
    def disconnect(self):
        if self.is_connected():
            self.device.Close()
        return not self.is_connected()

    def set_pulse_width(self, lamp, led, value):
        """Sets the pulse width of one LED"""
        index = (lamp * 8) + led
        self.device.SetWireInValue(0x11, index)
        self.device.SetWireInValue(0x10, value)
        self.apply()

    def set_intensity(self, lamp, led, value):
        """Sets the intensity of one LED"""
        index = (lamp * 8) + led + 8
        self.device.SetWireInValue(0x11, index)
        self.device.SetWireInValue(0x10, value)
        self.apply()

    def apply(self):
        """Applies the setting on the FPGA

        By sending the settings to the FPGA and triggering the execution
        """
        self.update()
        self.trigger()

    def trigger(self):
        """Triggers the execution of the new settings by the FPGA"""
        self.device.ActivateTriggerIn(0x40, 0)

    def update(self):
        """Send values to FPGA"""
        self.device.UpdateWireIns()


class ConnectionException(Exception):
    """Exception raised when dealing with OpalKelly connections"""

    pass


class FakeConnection(Connection):

    def __init__(self, bit_file, serial_nr=""):
        super(FakeConnection, self).__init__(bit_file=bit_file, serial_nr=serial_nr)
        self._connected = False

    def is_connected(self):
        return self._connected is True

    @Connection.thread_safe
    def connect(self, bit_file=None, serial_nr=None):
        result = False
        if bit_file is None:
            bit_file = self.bit_file
        if serial_nr is None:
            serial_nr = self.serial_nr
        if serial_nr is None:
            serial_nr = ""
        # result = self.device.OpenBySerial(serial_nr) >= 0
        self._connected = True
        self.serial_nr = serial_nr
        try:
            result = self.configure(bit_file)
        finally:
            if result is False:
                self.disconnect()
        return result

    @Connection.thread_safe
    def configure(self, bit_file):
        if bit_file is None:
            raise ConnectionException("No bit file set")
        if not os.path.exists(bit_file):
            raise ConnectionException("Bit file location does not exist")

    @Connection.thread_safe
    def disconnect(self):
        if self.is_connected():
            self._connected = False
        return not self.is_connected()
