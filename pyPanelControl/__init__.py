"""Application entry point"""
from julesTk.app import Application
from controller import MainController

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class PanelControlApp(Application):

    def _prepare(self):
        self.add_controller("main", MainController(self))
        self.wm_resizable(False, False)
        self.wm_title("LED Panel GUI")

    @property
    def main(self):
        return self.get_controller("main")

    def _start(self):
        self.main.start()


def start_app():
    app = PanelControlApp()
    app.run()
