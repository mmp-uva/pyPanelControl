==============
pyPanelControl
==============

A simple GUI to a FPGA driven LED Panel.

Requirements
------------

* *tkinter*
* *ttk*
* `julesTk`_

Installation
------------

1. Install the packages via pip

.. code-block:: bash

	# install julesTk dependency
	pip install git+https://github.com/jjongbloets/julesTk.git
	# install pyPanelControl
	pip install git+https://gitlab.com/mmp-uva/pyPanelControl.git

Running
-------

Upon installation, a small console script is generated. This script can be executed from the command-line::

	PanelControlGUI

Or via the python console

.. code-block:: python

	from pyPanelControl import start_app
	start_app()

FAQ
---

1. Python crashes with the following error on Mac OS X::

	Fatal Python error: PyThreadState_Get: no current thread
	Abort trap: 6

This behaviour is caused by a bad link between the shared object (_ok.so in pyPanelControl/ok) and the libpython.dylib.
The solution is to relink the shared object to the correct Python library location, using otool and install_name_tool

.. code-block::

	cd <location of python package>/ok
	otool -L _ok.so
	# returns list of linked libraries, on top is the libpythonX.X.dylib
	# locate correct location of library and rename using install_name_tool
	install_name_tool -change <old> <new> _ok.so


.. _julesTk: https://github.com/jjongbloets/julesTk
